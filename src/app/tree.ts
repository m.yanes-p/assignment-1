export class Tree {
	growth: number;
	fruitTaken: number;
	
	constructor(growth: number = 0, fruitTaken: number = 0) {
		this.growth = growth;
		this.fruitTaken = fruitTaken;
	}
	
	grow(growthValue: number = 1): void {
		this.growth += growthValue;
	}
	
	fruitAvailable(): number {
		return (Math.floor(Math.log10(this.growth))) - this.fruitTaken;
	}
	
	size(): number {
		return Math.pow(this.growth, (2/3));
	}
}

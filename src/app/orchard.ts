export class Orchard {
	size: number;
	trees: Tree[];
	
	constructor(size: number, trees: Tree[] = []) {
		this.size = size ;
		this.trees = trees;
	}
	
	addTree(t: tree) {
		this.trees.push(t);
	}
		
	totalFruitAvailable(): number {
		let sum: number = 0;
		
		this.trees.forEach(tree => tree.fruitAvailable() += sum);
		
		return sum;
	}
	
	canFit(t: tree): boolean {
		let sum: number = 0;
		let fits: boolean = false;
		
		this.trees.forEach(tree => tree.size() += sum);
		if((sum - this.size) >= t) {
			fits = true;
		}
		
		return fits;
	}
	
}

